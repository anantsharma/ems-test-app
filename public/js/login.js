$(document).ready(function(e){
    $("#sign-in-btn").click(function(e){
        $('.error').css('display', 'none');
        e.preventDefault();
        var data = {
            username : $("#user-id").val(),
            password : $("#password").val()
        };
        
        $.post('/login',data,function(response){
            console.dir(response);
            if(response == '1'){
                window.location.href = './';
            }else{
                $('.error').css('display', 'block');
            }
        });
        
    });
})