var express = require("express");
var morgan = require("morgan");
var session = require('express-session');
var bodyParser = require('body-parser');

var app = express();
app.set('view engine','ejs');

app.use(morgan('tiny'));
app.use(session({
    secret: 'secret',
    name: 'Cloudify',
    resave: true,
    saveUninitialized: true
}));
app.use('/public', express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

var users = [{
    username : 'admin',
    password : 'admin',
    name : 'Admin',
    role : 'Administrator'
},
{
    username : 'user',
    password : 'password',
    name : 'User',
    role : 'User'
},
{
    username : 'stafin',
    password : 'stafin',
    name : 'Stafin',
    role : 'Administrator'
}];

app.get('/',function(req, res){
    // res.render('index.ejs',{name : req.session.name, role : req.session.role});
    if(!req.session.username){
        res.redirect('/auth');
    }else{
        res.render('index.ejs',{name : req.session.name, role : req.session.role});
    }
});

app.get('/auth', function(req, res, next){
    // res.render('auth.ejs',{name : req.session.name, role : req.session.role});
    if(req.session.username){
        res.redirect('/');
    }else{
        res.render('auth.ejs');
    }
});

app.post('/login', function(req, res, next){
    var session = req.session;
    var username = req.body.username;
    var password = req.body.password;
    var user = {};
    var result = '0';
    
    for(var i=0; i<users.length; i++){
        user = users[i];
        if(username == user.username && password == user.password){
            session.username = username;
            session.name = user.name;
            session.role = user.role;
            result = '1';
            break;
        }
    }
    
    res.send(result);
    
});

app.get('/logout', function(req, res, next) {
    req.session.destroy();
    res.redirect('/');
});

app.listen(process.env.PORT || 8080, function(){
    console.log("Started on "+process.env.IP+":"+process.env.PORT);
});